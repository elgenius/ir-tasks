import sqlite3 as lite


class database:
    def __init__(self):
        self.__connection = self.__connectToDb()
        self.cur = self.__connection.cursor()
        self.create_db_tables()

        def __del__(self):
            self.cur.close()
            self.__connection.close()

    def __connectToDb(self):
        conn = lite.connect('ir_task1.db')
        conn.row_factory = self.QueryDictionaries
        return conn

    def GetConnection(self):
        return self.__connection

    def create_db_tables(self):
        self.cur.execute(
            'CREATE TABLE IF NOT EXISTS documents (id INTEGER PRIMARY KEY AUTOINCREMENT,path varchar(255), tokens TEXT NULL) ')
        self.cur.execute(
            'CREATE TABLE IF NOT EXISTS postlist  (id INTEGER PRIMARY KEY AUTOINCREMENT,term varchar(255) NULL, docs TEXT NULL) ')

    def execute(self, query):
        return self.cur.execute(query).fetchall()

    def add_document(self, path):
        # path should be unique so we will make a selection to check if it exists or not
        fetched_path = self.cur.execute('select * from documents where path = "' + path + '" ').fetchone()
        if fetched_path is not None and len(fetched_path) is not 0:
            # self.printDocs()
            return fetched_path["id"]
        self.cur.execute('INSERT INTO documents (path) values ("' + path + '") ').fetchone()
        Id = self.cur.lastrowid
        self.commit()
        # self.printDocs()
        return Id

    def add_token(self, id, token):
        self.cur.execute('UPDATE documents SET tokens= ? WHERE id = ? ', (token, str(id)))
        self.commit()
        # self.printDocs(id)

    def printDocs(self, id=None):
        if id is None:
            print(self.cur.execute('select * from documents').fetchall())
        else:
            print(self.cur.execute('select * from documents where id = ' + str(id) + ' ').fetchone())

    def printPostlist(self, id = None):
        if id is None:
            print(self.cur.execute('select * from postlist').fetchall())
        else:
            print(self.cur.execute('select * from postlist where id = ' + str(id) + ' ').fetchone())

    def commit(self):
        self.__connection.commit()

    def QueryDictionaries(self, cursor, row):
        """
        row factory which will allow me to retrieve query
        result in an associative array

        :param cursor:
        :param row:
        :return:
        """
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    # def MakePostingList(self, tokens = None, stopwords = None):
    #     self.cur.execute('INSERT INTO postlist ('') values ('') ')
    #     id = self.cur.lastrowid
    #     self.printPostlist()
    #     return id
