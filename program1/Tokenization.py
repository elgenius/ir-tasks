from program1 import database
from stemming.porter2 import stem
from program1 import Utils


class Tokenization(database):
    stop_words = {}

    def __init__(self):
        super().__init__()
        self.inter_words = {
            '.': 1,
            '\'': 1,
            '\"': 1,
            '!': 1,
            '~': 1,
            '$': 1,
            '^': 1,
            '&': 1,
            '|': 1,
            ';': 1,
            ':': 1,
            '<': 1,
            '>': 1,
            '?': 1,
            '+': 1,
            '=': 1,
            ',': 1,
            '`': 1,
            ' ': 1,
            '\n': 1,
            '{': 1,
            '}': 1,
            ')': 1,
            '(': 1,
            '*': 1,
        }
        self.tokens = {}
        self.utils = Utils.Utils()
        self.document_stop_words = {}

    def FilterText(self, doc):
        doc_buffer = doc.document
        doc_text = doc_buffer.readlines()
        current_text = ""
        self.LoadStopWords()
        for line in doc_text:
            for character in line:
                try:
                    self.inter_words[character]
                except KeyError:
                    current_text += character.lower()

                if character is ' ':
                    try:
                        t = Tokenization.stop_words[current_text]
                        self.document_stop_words[current_text] = doc.document_id
                    except KeyError:
                        self.tokens[stem(current_text)] = doc.document_id
                        # self.tokens.append(stem(current_text))
                    current_text = ""
        return sorted(list(self.tokens))

    def LoadStopWords(self):
        if not len(Tokenization.stop_words):
            file = open('helper_docs/stopwords.txt')
            for word in file.readlines():
                Tokenization.stop_words[word.rstrip('\n')] = 1
        return Tokenization.stop_words
