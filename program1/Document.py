from program1 import database
from program1 import Tokenization
import json


class Document(database):
    def __init__(self, path, exist=0):
        super().__init__()
        self.AddDocument(path)
        self.document = self.OpenFileForRead(path)
        self.path = path
        self.Tokenize = Tokenization.Tokenization()
        self.TokenizeDocument()
        self.document_id = ""
        self.tokens = ""

    def __del__(self):
        self.document.close()

    def AddDocument(self, path):
        self.document_id = self.add_document(path)

    def OpenFileForRead(self, path):
        return open(path)

    def EraseAndPutInFile(self, text):
        self.document = open(self.path, 'w')
        self.document.write(text)

    def TokenizeDocument(self):
        token_obj = {"tokens": self.Tokenize.FilterText(self)}
        tokens = json.dumps(token_obj)
        self.add_token(self.document_id, tokens)

    def GetStopWords(self):
        return self.Tokenize.document_stop_words

    def GetDocumentTokens(self):
        return self.Tokenize.tokens
